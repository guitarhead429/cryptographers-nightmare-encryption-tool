
public class VignereEncrypt {
	private int[] toEncode;
	private int[] encodedMsg;
	private int[] key;
	public VignereEncrypt(int[] toEncode, int[] key)
	{
		this.toEncode = toEncode;
		this.key = key;
	}
	
	public int[] makeKeyList()
	{
		int[] keyList = new int[toEncode.length];
		int kCounter = 0;
		for (int k = 0; k < toEncode.length; k++)
		{
			if (kCounter == 3) kCounter = 0;
			keyList[k] = key[kCounter++];
			System.out.print(keyList[k] + " ");
		}
		System.out.println();
		return keyList;
	}
	
	public int mod(int a, int c)
	{
		double b = a;
		double m = c;
		int ans = (int) (b - m * Math.floor(b / m));
		return ans;
	}
	
	public int[] encrypt()
	{
		int[] keyList = makeKeyList();
		encodedMsg = new int[toEncode.length];
		for (int m = 0; m < toEncode.length; m++)
		{
			if (toEncode[m] != -1)
			{
				encodedMsg[m] = toEncode[m] + keyList[m] % 26;
			}
			else
			{
				toEncode[m] = -1;
			}
		}
		return encodedMsg;
	}
	
	public int[] decrypt()
	{
		int[] keyList = makeKeyList();
		encodedMsg = toEncode;
		for (int m = 0; m < encodedMsg.length; m++)
		{
			if (encodedMsg[m] > -1)
			{
				int subtraction = encodedMsg[m] - keyList[m];
				encodedMsg[m] = mod(subtraction, 26);
				//System.out.print(mod(subtraction, 26) + " ");
			}
		}
		return encodedMsg;
	}
	
	public void print()
	{
		System.out.println("Result: ");
		for (int j = 0; j < encodedMsg.length; j++)
		{
			if(encodedMsg[j] != -1)
			{
				System.out.print(encodedMsg[j]);
				System.out.print(" ");
			}
			else
			{
				System.out.print("_ ");
			}
		}
	}
}
