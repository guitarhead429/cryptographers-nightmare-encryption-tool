import java.util.Scanner;


public class DecryptMain {

	public static void main(String[] args) {
		int[] encodedMsg;
		System.out.println("Enter a file name:");
		Scanner in = new Scanner(System.in);
		String filenameInput = in.nextLine();
		FileManager fm = new FileManager();
		encodedMsg = fm.loadFromFile(filenameInput);
		int[] decodedMsg = new int[encodedMsg.length];

		CaesarEncrypt ce = new CaesarEncrypt(encodedMsg);
		ROT13Encrypt rt = new ROT13Encrypt(encodedMsg);
		AtbashEncrypt ab = new AtbashEncrypt(encodedMsg);
		System.out.println("Select an decryption method:");
		System.out.println("1.) Shift Decryption");
		System.out.println("2.) ROT13 Decryption");
		System.out.println("3.) Atbash Decryption");
		System.out.println("4.) Vigenere Decryption");
		
		int selection = -1;
		while (in.hasNext())
		{
			selection = in.nextInt();
			if (selection == 1)
			{
				System.out.println("Shift Decryption selected.");
				decodedMsg = ce.decrypt();
				interpretNumbers it = new interpretNumbers(decodedMsg);
				it.interpretNumbersToPlaintext();
				it.printInterpretation();
				System.out.println(" ");
				break;
			}
			else if(selection == 2)
			{
				System.out.println("ROT13 Decryption selected.");
				decodedMsg = rt.decrypt();
				interpretNumbers it = new interpretNumbers(decodedMsg);
				it.interpretNumbersToPlaintext();
				it.printInterpretation();
				System.out.println(" ");
				break;
			}
			else if(selection == 3)
			{
				System.out.println("Atbash Decryption selected.");
				decodedMsg = ab.decrypt();
				interpretNumbers it = new interpretNumbers(decodedMsg);
				it.interpretNumbersToPlaintext();
				it.printInterpretation();
				System.out.println(" ");
				break;
			}
			else if(selection == 4)
			{
				System.out.println("Vigenere Decryption selected. Please enter the appropriate decryption key:");
				String key = in.next();
				interpretText its = new interpretText(key);
				int[] keyword = its.interpretPlainTextToNumbers();
				
				VignereEncrypt ve = new VignereEncrypt(encodedMsg, keyword);
				decodedMsg = ve.decrypt();
				interpretNumbers it = new interpretNumbers(decodedMsg);
				it.interpretNumbersToPlaintext();
				it.printInterpretation();
				System.out.println(" ");
				break;
			}
			else 
			{
				break;
			}
			
		}
	}
}

