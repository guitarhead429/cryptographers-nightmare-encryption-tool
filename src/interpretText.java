
public class interpretText {
	String[] plainText;
	String toEncode;
	int[] encodedMsg;
	public interpretText(String toEncode)
	{
		this.toEncode = toEncode;
		plainText = new String[toEncode.length()];
		for(int k = 0; k < toEncode.length(); k++) // get individual characters to analyze later
		{
			this.plainText[k] = Character.toString(toEncode.charAt(k));
			//System.out.print(plainText[k]);
		}
	}
	
	public int[] interpretPlainTextToNumbers()
	{
		this.encodedMsg = new int[this.toEncode.length()];
		for(int i = 0; i < plainText.length; i++) //convert to number format
		{
			if(plainText[i].equalsIgnoreCase(" ")){
				encodedMsg[i] = -1;
			}
			else if(plainText[i].equalsIgnoreCase("a")){
				encodedMsg[i] = 0;
			}
			else if(plainText[i].equalsIgnoreCase("b")){
				encodedMsg[i] = 1;
			}
			else if(plainText[i].equalsIgnoreCase("c")){
				encodedMsg[i] = 2;
			}
			else if(plainText[i].equalsIgnoreCase("d")){
				encodedMsg[i] = 3;
			}
			else if(plainText[i].equalsIgnoreCase("e")){
				encodedMsg[i] = 4;
			}
			else if(plainText[i].equalsIgnoreCase("f")){
				encodedMsg[i] = 5;
			}
			else if(plainText[i].equalsIgnoreCase("g")){
				encodedMsg[i] = 6;
			}
			else if(plainText[i].equalsIgnoreCase("h")){
				encodedMsg[i] = 7;
			}
			else if(plainText[i].equalsIgnoreCase("i")){
				encodedMsg[i] = 8;
			}
			else if(plainText[i].equalsIgnoreCase("j")){
				encodedMsg[i] = 9;
			}
			else if(plainText[i].equalsIgnoreCase("k")){
				encodedMsg[i] = 10;
			}
			else if(plainText[i].equalsIgnoreCase("l")){
				encodedMsg[i] = 11;
			}
			else if(plainText[i].equalsIgnoreCase("m")){
				encodedMsg[i] = 12;
			}
			else if(plainText[i].equalsIgnoreCase("n")){
				encodedMsg[i] = 13;
			}
			else if(plainText[i].equalsIgnoreCase("o")){
				encodedMsg[i] = 14;
			}
			else if(plainText[i].equalsIgnoreCase("p")){
				encodedMsg[i] = 15;
			}
			else if(plainText[i].equalsIgnoreCase("q")){
				encodedMsg[i] = 16;
			}
			else if(plainText[i].equalsIgnoreCase("r")){
				encodedMsg[i] = 17;
			}
			else if(plainText[i].equalsIgnoreCase("s")){
				encodedMsg[i] = 18;
			}
			else if(plainText[i].equalsIgnoreCase("t")){
				encodedMsg[i] = 19;
			}
			else if(plainText[i].equalsIgnoreCase("u")){
				encodedMsg[i] = 20;
			}
			else if(plainText[i].equalsIgnoreCase("v")){
				encodedMsg[i] = 21;
			}
			else if(plainText[i].equalsIgnoreCase("w")){
				encodedMsg[i] = 22;
			}
			else if(plainText[i].equalsIgnoreCase("x")){
				encodedMsg[i] = 23;
			}
			else if(plainText[i].equalsIgnoreCase("y")){
				encodedMsg[i] = 24;
			}
			else if(plainText[i].equalsIgnoreCase("z")){
				encodedMsg[i] = 25;
			}
		}
		return encodedMsg;
	}
	
	public void printInterpretation()
	{
		System.out.println("Interpreted message: ");
		for (int j = 0; j < encodedMsg.length; j++)
		{
			if(encodedMsg[j] != -1)
			{
				System.out.print(encodedMsg[j]);
				System.out.print(" ");
			}
			else
			{
				System.out.print("_ ");
			}
		}
	}
}
