
public class CaesarEncrypt {
	int[] toEncode;
	int[] encodedMsg;
	public CaesarEncrypt(int[] toEncode)
	{
		this.toEncode = toEncode;
	}
	
	public int[] encrypt()
	{
		encodedMsg = new int[toEncode.length];
		for (int m = 0; m < toEncode.length; m++) //Perform shift
		{
			if(toEncode[m] != -1)
			{
				encodedMsg[m] = (toEncode[m] + 3) % 26; 
			}
			else
			{
				encodedMsg[m] = -1;
			}
		}
		return encodedMsg;
	}
	
	public int[] decrypt()
	{
		this.encodedMsg = toEncode;
		for (int m = 0; m < encodedMsg.length; m++) //Perform de-shift
		{
			if(encodedMsg[m] > -1)
			{
				encodedMsg[m] = (encodedMsg[m] - 3) % 26;
				if (encodedMsg[m] == -3) // Hard fix for a potential bug in Java code/Potential improper use of Java's mod operation.
				{
					encodedMsg[m] = 23;
				}
				else if(encodedMsg[m] == -2)
				{
					encodedMsg[m] = 24;
				}
				else if(encodedMsg[m] == -1)
				{
					encodedMsg[m] = 25;
				}
				//System.out.println(encodedMsg[m]);
			}
		}
		return encodedMsg;
	}
	
	public void print()
	{
		System.out.println("Result: ");
		for (int j = 0; j < encodedMsg.length; j++)
		{
			if(encodedMsg[j] != -1)
			{
				System.out.print(encodedMsg[j]);
				System.out.print(" ");
			}
			else
			{
				System.out.print("_ ");
			}
		}
	}
}
