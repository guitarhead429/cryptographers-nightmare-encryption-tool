import java.util.Scanner;


public class main {

	public static void main(String[] args) {
		System.out.println("Enter your text: ");
		String input = " ";
		Scanner in = new Scanner(System.in);
		input = in.nextLine();
		interpretText it = new interpretText(input);
		int[] toEncode = it.interpretPlainTextToNumbers();
		int[] encodedMsg = new int[toEncode.length];
		it.printInterpretation();
		System.out.println(" ");
		System.out.println(" ");
		
		CaesarEncrypt ce = new CaesarEncrypt(toEncode);
		ROT13Encrypt rt = new ROT13Encrypt(toEncode);
		AtbashEncrypt ab = new AtbashEncrypt(toEncode);
		System.out.println("Select an encryption method:");
		System.out.println("1.) Shift Encryption");
		System.out.println("2.) ROT13 Encryption");
		System.out.println("3.) Atbash Encryption");
		System.out.println("4.) Vigenere Encryption");

		int selection = -1;
		while (in.hasNext())
		{
			selection = in.nextInt();
			if (selection == 1)
			{
				System.out.println("Shift Encryption selected.");
				encodedMsg = ce.encrypt();
				ce.print();
				System.out.println(" ");
				break;
			}
			else if(selection == 2)
			{
				System.out.println("ROT13 Encryption selected.");
				encodedMsg = rt.encrypt();
				rt.print();
				System.out.println(" ");
				break;
			}			
			else if(selection == 3)
			{
				System.out.println("Atbash Encryption selected.");
				encodedMsg = ab.encrypt();
				ab.print();
				System.out.println(" ");
				break;
			}
			else if(selection == 4)
			{
				System.out.println("Vigenere Encryption selected. Please enter a key word by which to encode and decode your message:");
				String key = in.next();
				it = new interpretText(key);
				int[] keyword = it.interpretPlainTextToNumbers();
				
				VignereEncrypt ve = new VignereEncrypt(toEncode, keyword);
				encodedMsg = ve.encrypt();
				ve.print();
				System.out.println(" ");
				break;
			}
			else 
			{
				break;
			}
			
		}
		
		String input2 = " ";
		System.out.println("Save encrypted message to text file? y/n");
		while (in.hasNext())
		{
			input2 = in.next();
			if (input2.equalsIgnoreCase("y"))
			{
				FileManager fm = new FileManager();
				System.out.println("Please enter a file name:");
				fm.saveToFile(encodedMsg);
				System.out.println("Encrypted message saved to text file.");
				break;
			}
			else if (input2.equalsIgnoreCase("n"))
			{
				System.out.println("Program terminated.");
				break;
			}
		}
		in.close();
	}

}
