
public class ROT13Encrypt {
	int[] toEncode;
	int[] encodedMsg;
	public ROT13Encrypt(int[] toEncode)
	{
		this.toEncode = toEncode;
	}
	
	public int[] encrypt()
	{
		encodedMsg = new int[toEncode.length];
		for (int m = 0; m < toEncode.length; m++) //Perform shift
		{
			if(toEncode[m] != -1)
			{
				encodedMsg[m] = (toEncode[m] + 13) % 26; 
			}
			else
			{
				encodedMsg[m] = -1;
			}
		}
		return encodedMsg;
	}
	
	public int[] decrypt()
	{
		encodedMsg = toEncode;
		for (int m = 0; m < encodedMsg.length; m++) //Perform de-shift
		{
			if(encodedMsg[m] > -1)
			{
				encodedMsg[m] = (encodedMsg[m] + 13) % 26; 
			}
		}
		return encodedMsg;
	}
	
	public void print()
	{
		System.out.println("Result: ");
		for (int j = 0; j < encodedMsg.length; j++)
		{
			if(encodedMsg[j] != -1)
			{
				System.out.print(encodedMsg[j]);
				System.out.print(" ");
			}
			else 
			{
				System.out.print("_ ");
			}
		}
	}
}
