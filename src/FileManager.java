import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;


public class FileManager {
	int[] encodedMsg;
	ArrayList<Integer> fromFile;
	public FileManager()
	{
		fromFile = new ArrayList<Integer>();
	}
	
	public String getFileNameFromUser()
	{
		String filename = "";
		Scanner in = new Scanner(System.in);
		filename = in.nextLine();
		filename += ".txt";
		in.close();
		return filename;
	}
	
	public String saveToFile(int[] encodedMsg)
	{
		String filename = getFileNameFromUser();
		try
		{
			PrintWriter w = new PrintWriter(filename);
			for(int k = 0; k < encodedMsg.length; k++)
			{
				if(encodedMsg[k] >= -1)
				{
					w.println(encodedMsg[k]);
				}
			}
			w.close();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public int[] convertArrayListtoArray()
	{
		encodedMsg = new int[fromFile.size()];
		for(int i = 0; i < encodedMsg.length; i++)
		{
			encodedMsg[i] = fromFile.get(i);
			//System.out.print(encodedMsg[i] + " ");
		}
		return encodedMsg;
	}
	
	public int[] loadFromFile(String filename)
	{
		File inputFile = new File(filename);
		try
		{
			Scanner in = new Scanner(inputFile);
			while(in.hasNextLine())
			{
				fromFile.add((Integer.parseInt(in.nextLine())));
				//System.out.println(fromFile);
			}
			in.close();
			convertArrayListtoArray();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		return encodedMsg;
	}
}
