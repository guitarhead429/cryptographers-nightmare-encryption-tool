
public class AtbashEncrypt {
	int[] toEncode;
	int[] encodedMsg;
	public AtbashEncrypt(int[] toEncode)
	{
		this.toEncode = toEncode;
	}
	
	public int[] encrypt()
	{
		encodedMsg = new int[toEncode.length];
		for(int m = 0; m < toEncode.length; m++)
		{
			if(toEncode[m] != -1)
			{
				encodedMsg[m] = (26 - (toEncode[m] + 1));
			}
			else
			{
				encodedMsg[m] = -1;
			}
		}
		return encodedMsg;
	}
	
	public int[] decrypt()
	{
		this.encodedMsg = toEncode;
		for(int m = 0; m < encodedMsg.length; m++)
		{
			if(encodedMsg[m] > -1)
			{
				encodedMsg[m] = (25 - (encodedMsg[m]));
			}
		}
		return encodedMsg;
	}
	
	public void print()
	{
		System.out.println("Result: ");
		for (int j = 0; j < encodedMsg.length; j++)
		{
			if(encodedMsg[j] != -1)
			{
				System.out.print(encodedMsg[j]);
				System.out.print(" ");
			}
			else
			{
				System.out.print("_ ");
			}
		}
	}
}
