
public class interpretNumbers {
	int[] encodedMsg;
	String[] plainText;
	public interpretNumbers(int[] encodedMsg)
	{
		this.encodedMsg = encodedMsg;
	}
	
	public String[] interpretNumbersToPlaintext()
	{
		this.plainText = new String[this.encodedMsg.length];
		for (int i = 0; i < encodedMsg.length; i++)
		{
			if(encodedMsg[i]==(-1)){
				plainText[i] = " ";
			}
			else if(encodedMsg[i]==(0)){
				plainText[i] = "a";
			}
			else if(encodedMsg[i]==(1)){
				plainText[i] = "b";
			}
			else if(encodedMsg[i]==(2)){
				plainText[i] = "c";
			}
			else if(encodedMsg[i]==(3)){
				plainText[i] = "d";
			}
			else if(encodedMsg[i]==(4)){
				plainText[i] = "e";
			}
			else if(encodedMsg[i]==(5)){
				plainText[i] = "f";
			}
			else if(encodedMsg[i]==(6)){
				plainText[i] = "g";
			}
			else if(encodedMsg[i]==(7)){
				plainText[i] = "h";
			}
			else if(encodedMsg[i]==(8)){
				plainText[i] = "i";
			}
			else if(encodedMsg[i]==(9)){
				plainText[i] = "j";
			}
			else if(encodedMsg[i]==(10)){
				plainText[i] = "k";
			}
			else if(encodedMsg[i]==(11)){
				plainText[i] = "l";
			}
			else if(encodedMsg[i]==(12)){
				plainText[i] = "m";
			}
			else if(encodedMsg[i]==(13)){
				plainText[i] = "n";
			}
			else if(encodedMsg[i]==(14)){
				plainText[i] = "o";
			}
			else if(encodedMsg[i]==(15)){
				plainText[i] = "p";
			}
			else if(encodedMsg[i]==(16)){
				plainText[i] = "q";
			}
			else if(encodedMsg[i]==(17)){
				plainText[i] = "r";
			}
			else if(encodedMsg[i]==(18)){
				plainText[i] = "s";
			}
			else if(encodedMsg[i]==(19)){
				plainText[i] = "t";
			}
			else if(encodedMsg[i]==(20)){
				plainText[i] = "u";
			}
			else if(encodedMsg[i]==(21)){
				plainText[i] = "v";
			}
			else if(encodedMsg[i]==(22)){
				plainText[i] = "w";
			}
			else if(encodedMsg[i]==(23)){
				plainText[i] = "x";
			}
			else if(encodedMsg[i]==(24)){
				plainText[i] = "y";
			}
			else if(encodedMsg[i]==(25)){
				plainText[i] = "z";
			}
		}
		return plainText;
	}
	
	public void printInterpretation()
	{
		System.out.println("Interpreted message: ");
		for (int j = 0; j < encodedMsg.length; j++)
		{
			if(encodedMsg[j] != -1)
			{
				System.out.print(plainText[j]);
				System.out.print(" ");
			}
			else
			{
				System.out.print("_ ");
			}
		}
	}
}
