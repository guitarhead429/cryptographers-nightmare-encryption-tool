Cryptographer's Nightmare Text Encryption Tool
---------------------------------///
To run TextEncrypt, type the following into the Windows command line:

	java -jar TextEncrypt.jar

The same command goes for TextDecrypt.jar.

v.1.2
------/
-Full implementation of the Vignere cipher added. You can now encrypt and decrypt text using a special keyword. THIS KEYWORD IS NOT SAVED BY THE PROGRAM IN ANY WAY. If you forget the keyword, then the text may not be decipherable
by any convenient means.

v.1.1
------/
-Fixed improper/negligent handling of spaces in the user-provided text. Program now shows underscores where spaces are located. Spaces are now also saved properly inside the text files (they are represented by the value -1).
-New encryption type Atbash has been fully implemented. Vignere has yet to be started.

v1.0
------/
-Two text encryption choices available - Shift and ROT13.
-Vignere ciphers have yet to be implemented. *Coming soon - Atbash cipher encoding*
-TextEncrypt takes any formation of text A through Z, including spaces. It does not take into account numbers, punctuation, or other special characters. This will likely change soon.
-When saving encrypted text to disk, you will be prompted for a filename of your choosing - type just the name, leave out ".txt" or any other file extention - the program handles it for you and automatically saves as a basic text file.
-TextDecrypt will initially ask you for the name of text file to read - make sure you include the file extension ".txt" otherwise the program will return a FileNotFound exception. This may or may not change as time goes on.


